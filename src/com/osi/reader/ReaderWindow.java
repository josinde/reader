/*
 * Copyright (C) 2006-2011 Gaia Data Processing and Analysis Consortium
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
package com.osi.reader;



import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;


public class ReaderWindow extends JDialog {

    private static long INITIAL_TIMER_DELAY = 500L;
    
    private static int INITIAL_SPEED = 400;
    
    /** Timer timer ... el temporizador, claro */
    private Timer timer;
    
    /** Data origin */
    private JTextArea textarea;
    
    /** Custom read panel */
    private ReaderPanel panel;
    
	/** Constructor */
	ReaderWindow(MainFrame parent, JTextArea textarea) {
	    super((JFrame) null, "Reader", false);
        this.textarea = textarea;
        // Layout
        initLayout();
        // Close window listener
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                pause(true);
            }
        });
	}
	
    /** layout */
	private void initLayout() {
		
        setLayout(new GridBagLayout());
        setSize(new Dimension(500, 150));
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);
        
        // CAD id (not editable)
        this.panel = new ReaderPanel(this);
        panel.setBackground(Color.WHITE);
        panel.setForeground(Color.BLACK);

        this.add(panel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                new Insets(10, 4, 2, 4), 0, 0));   
        
        // Buttons
		JPanel buttonsPanel = getButtonsPanel();
        this.add(buttonsPanel, new GridBagConstraints(0, 10, 2, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                new Insets(2, 4, 2, 4), 0, 0));         
		
	}

	/** Setup the timer, only if needed */
    private void setTimer() {
        if (timer == null) {
            // Get period
            int speed = slider.getValue(); 
            long period = speed2Period(speed);
            // Set new timer
            this.timer = new Timer();       
            timer.scheduleAtFixedRate(new CustomTimerTask(), INITIAL_TIMER_DELAY, period);
        }
    }
    
    /** Cancel and remove current timer */
    private void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
	
	private JSlider slider;
	private JLabel sliderLabel;
	
	private JToggleButton runButton;
	
    private JPanel getButtonsPanel() {
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridBagLayout());
        
        this.runButton = new JToggleButton("READ");
        runButton.setPreferredSize(new Dimension(100, 25));
        //runButton.setMinimumSize(new Dimension(100, 25));
        //runButton.setMaximumSize(new Dimension(100, 25));
        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isRunning()) {
                    resume();
                } else {
                    pause(true);
                }
            }
        });
        buttonsPanel.add(runButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                new Insets(2, 2, 2, 2), 0, 0));
        
        JButton setButton = new JButton("GOTO");
        setButton.setPreferredSize(new Dimension(100, 25));
        //setButton.setMinimumSize(new Dimension(100, 25));
        //setButton.setMaximumSize(new Dimension(100, 25));
        setButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // First stop reading 
                // IMPORTANT: Don't update the text area, or we loose the caret position
                pause(false);
                // Load new text
                reload();
            }
        });
        
        buttonsPanel.add(setButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                new Insets(2, 2, 2, 2), 0, 0));         
        
        // Slider
        this.slider = new JSlider(JSlider.HORIZONTAL, 100, 999, INITIAL_SPEED);
        //Turn on labels at major tick marks.
        slider.setMajorTickSpacing(50);
        slider.setMinorTickSpacing(10);
        slider.setPaintTicks(false);
        slider.setPaintLabels(false);
        slider.addChangeListener(new MyChangeAction());
        buttonsPanel.add(slider, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                new Insets(2, 2, 2, 2), 0, 0));         
        
        this.sliderLabel = new JLabel(Integer.toString(slider.getValue()));

        buttonsPanel.add(sliderLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                new Insets(2, 2, 2, 2), 0, 0));         

        return buttonsPanel;
    }
    
    private int caretposition = 0;
    
    /** Get the text from the textarea starting at the caret position */
    public void reload() {
        try {

            // Remove any Highlight
            textarea.getHighlighter().removeAllHighlights();
            
            this.caretposition = textarea.getCaretPosition();
            int size = textarea.getDocument().getLength();
            
            //System.out.println("caretposition: " + caretposition);
            //System.out.println("size         : " + size);
            
            int len = size - caretposition;
            String text = textarea.getText(caretposition, len);
            panel.setText(text);
            
            // First word already loaded
            feedbackReadPosition();
            
            return;
            
        } catch (BadLocationException e) {
            e.printStackTrace();
            panel.setText("PROBLEMS!");
        }
    }
    
    private static DefaultHighlighter.DefaultHighlightPainter H_PAINER = 
            new DefaultHighlighter.DefaultHighlightPainter(new Color(200, 200, 100));

    
    /** Mark in the text area current position */
    private void feedbackReadPosition() {
        
        // Remove any Highlight
        Highlighter highlighter = textarea.getHighlighter();
        highlighter.removeAllHighlights();
        
        int bytesRead = panel.getBytesRead();
        
        if (bytesRead > 0) try {
            
            int position = caretposition + bytesRead - 1; 
            
            //System.out.println("caretposition: " + caretposition);
            //System.out.println("bytesRead    : " + bytesRead);
            
            // Go to the beginning of the current word
            int previous = position - panel.getCurrentWord().length();
            
            // highlight
            highlighter.addHighlight(previous, position, H_PAINER);
            
            // caret position
            textarea.setCaretPosition(position);
            
        } catch (BadLocationException ex) {
            ex.printStackTrace();
        }
    }
    
    private void resume() {
        // Button
        runButton.setText("PAUSE");
        runButton.setSelected(true);
        // Enables the timer, if not done yet
        setTimer();
}
    
    void pause(boolean feedbackPosition) {
        // Button
        runButton.setText("READ");
        runButton.setSelected(false);
        // feedback current position into the text area
        if (feedbackPosition) feedbackReadPosition();
    }
    
    private boolean isRunning() {
        return runButton.isSelected();
    }
    
    public class MyChangeAction implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent ce){
            // Shows new period
            int speed = slider.getValue();
            sliderLabel.setText(Integer.toString(speed));
            // Stop reading and reset timer
            cancelTimer();
            pause(true);
        }
    }    
    
    private long speed2Period(long speed) {
        return 60000L/speed;
    }
    /** Timer task that shows a new word only if enabled */
    private class CustomTimerTask extends TimerTask {
        @Override
        public void run() {
            if (isRunning()) {
                panel.showNextWord();
            }
        }
    }
}
