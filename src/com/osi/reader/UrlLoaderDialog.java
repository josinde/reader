package com.osi.reader;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;


class UrlLoaderDialog extends JDialog {

    private MainFrame parent;
    
    private JTextField urlField;
    
    /** constructor */
    UrlLoaderDialog(MainFrame parent) {
        super((Frame) null, "Loading a URL", true);
        this.parent = parent;
        
        initLayout();
    }
    
    /** layout */
    private void initLayout() {
        
        setLayout(new GridBagLayout());
        setSize(new Dimension(800, 100));
        setLocationRelativeTo(null);
        setResizable(false);
        
        JLabel label = new JLabel("URL:"); 
        this.add(label, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, 
                new Insets(20, 10, 4, 2), 0, 0));    
        
        this.urlField = new JTextField();
        urlField.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new MySwingWorker().execute();
            }});
        
        this.add(urlField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, 
                new Insets(20, 2, 4, 10), 0, 0));  
        
        // ...
        this.add(new JLabel(""), new GridBagConstraints(0, 10, 3, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                new Insets(0, 0, 0, 0), 0, 0));  
    }
    
    private class MySwingWorker extends SwingWorker<Void, String> {
       
        @Override
        protected Void doInBackground() throws Exception {

            waitUntilVisible();
            
            String url = urlField.getText();
            URI uri = URI.create(url);         

            publish("Loading: " + uri);
            parent.load(uri);
            publish("Page loaded");
            
            return null;
        }
        
        private void waitUntilVisible() {
            while (!UrlLoaderDialog.this.isVisible()) {
                waitABit();
            }
        }
        
        /** Let GUI update if needed */
        private void waitABit() {
            try {Thread.sleep(100);} catch (Exception ex) {}
        }

        @Override
        protected void process(List<String> chunks) {
            for (String msg : chunks) {
                System.out.println(msg);
            }
        }
        
        @Override
        protected void done() {
            UrlLoaderDialog.this.dispose();
        }
    }
}
