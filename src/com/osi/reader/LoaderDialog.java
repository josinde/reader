package com.osi.reader;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URI;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingWorker;


class LoaderDialog extends JDialog {

    private MainFrame parent;
    
    /** constructor */
    LoaderDialog(MainFrame parent, URI uri) {
        super((Frame) null, "Loading ...", true);
        this.parent = parent;
        
        initLayout(uri);
        
        MySwingWorker worker = new MySwingWorker(uri);
        worker.execute();
    }
    
    /** layout */
    private void initLayout(URI uri) {
        
        setLayout(new GridBagLayout());
        setSize(new Dimension(600, 100));
        setLocationRelativeTo(null);
        setResizable(false);
        
        JLabel label = new JLabel("Loading: " + uri + " ..."); 
        this.add(label, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                new Insets(20, 20, 10, 10), 0, 0));    
        
    }
    
    private class MySwingWorker extends SwingWorker<Void, String> {
       
        private URI uri;
        
        /** constructor */
        public MySwingWorker(URI uri) {
            this.uri = uri;
        }
        
        @Override
        protected Void doInBackground() throws Exception {

            waitUntilVisible();
            
            publish("Loading: " + uri);
            
            parent.loading(uri);

            publish("Page loaded");                   
            return null;
        }
        
        private void waitUntilVisible() {
            while (!LoaderDialog.this.isVisible()) {
                waitABit();
            }
        }
        
        /** Let GUI update if needed */
        private void waitABit() {
            try {Thread.sleep(100);} catch (Exception ex) {}
        }

        @Override
        protected void process(List<String> chunks) {
            for (String msg : chunks) {
                System.out.println(msg);
            }
        }
        
        @Override
        protected void done() {
            LoaderDialog.this.dispose();
        }
    }
}
