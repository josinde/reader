package com.osi.reader;


/*
 * ColorTransferHandler.java is used by the 1.4
 * DragColorDemo.java and DragColorTextFieldDemo examples.
 */

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.net.URI;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

/**
 * An implementation of TransferHandler that adds support for dropping
 * colors. Dropping a color on a component having this TransferHandler
 * changes the foreground or the background of the component to the dropped
 * color, according to the value of the changesForegroundColor property.
 */
class UrlTransferHandler extends TransferHandler {
	
    //String mimeType = DataFlavor.javaJVMLocalObjectMimeType + ";class=java.awt.Color";
	private DataFlavor myFlavor = DataFlavor.stringFlavor;

	private MainFrame target;
	
	
	/** constructor */
    UrlTransferHandler(MainFrame target) {
    	this.target = target;
    }

    /**
     * Overridden to import a Color if it is available.
     * getChangesForegroundColor is used to determine whether
     * the foreground or the background color is changed.
     */
    public boolean importData(JComponent c, Transferable t) {
    	
    	// Component is textarea but I'm not using it
    	
        if (hasStringFlavor(t.getTransferDataFlavors())) {
            try {
            	String transferData = (String) t.getTransferData(myFlavor);
            	transferData = transferData.trim();
            	if (transferData.startsWith("http") || transferData.startsWith("file")) {
            	    // URI
                    System.out.println("transfer: " + transferData + "<-");
            	    URI uri = URI.create(transferData);
            	    target.load(uri);
            	} else {
            	    // Regular text
            	    target.setText(transferData);
            	}
                return true;
            } catch (UnsupportedFlavorException ufe) {
                System.out.println("importData: unsupported data flavor");
                ufe.printStackTrace();
            } catch (IOException ioe) {
                System.out.println("importData: I/O exception");
                ioe.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Does the flavor list have a Color flavor?
     */
    protected boolean hasStringFlavor(DataFlavor[] flavors) {
        for (DataFlavor f : flavors) {
        	//System.out.println("flavor: " + f);
            if (f.equals(myFlavor)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Overridden to include a check for a color flavor.
     */
    public boolean canImport(JComponent c, DataFlavor[] flavors) {
        return hasStringFlavor(flavors);
    }
}

