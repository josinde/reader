package com.osi.reader;


import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;




class MainFrame extends JFrame {

    final static Charset ENCODING = StandardCharsets.UTF_8;
    
    private JTextArea textarea;
    
    private ReaderWindow reader;
    
    /** constructor */
    MainFrame() { 
        super("Reader");
        // Own layout
        initLayout();
    }
    
    private void initLayout() {
        
        // Frame setup
        this.setSize(600, 800);
        this.setJMenuBar(createMenuBar());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Frame content
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new GridBagLayout());
        
        // Create a TextArea to display the contents of the file in
        this.textarea = new JTextArea();
        textarea.setFont(new Font("MonoSpaced", Font.PLAIN, 12));
        textarea.setLineWrap(true);
        textarea.setCaretColor(Color.BLUE);
        
        this.add(new JScrollPane(textarea), new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
                new Insets(2, 2, 2, 2), 0, 0)); 
        
        // D&D
        //Create the URL transfer handler.
        UrlTransferHandler urlHandler = new UrlTransferHandler(this);
        textarea.setTransferHandler(urlHandler);
    }
    
    private void showReaderWindow() {
        if (reader == null) {
            this.reader = new ReaderWindow(this, textarea);
        }
        reader.setVisible(true);
    }
    
    /**
     * Creates the frame menu bar.
     * 
     * @return the menu bar
     */
    private JMenuBar createMenuBar() {
        JMenuBar menu = new JMenuBar();
        menu.add(createToolsMenu());
        return menu;
    }    
    
    private JMenu createToolsMenu() {

        JMenu menu = new JMenu("Tools");

        menu.add("Load text document").addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        // Opens a file chooser
                        JFileChooser chooser = new JFileChooser();
                        // Filter
//                        FileNameExtensionFilter filter = 
//                                new FileNameExtensionFilter("Text file", "txt");
//                        chooser.setFileFilter(filter);

                        int returnVal = chooser.showOpenDialog(MainFrame.this);
                        
                        if(returnVal == JFileChooser.APPROVE_OPTION) {
                            File file = chooser.getSelectedFile();
                            load(file.toURI());
                        }
                    }
                });       
        
        menu.add("Load pdf document").addActionListener(
                new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        // Opens a file chooser
                        JFileChooser chooser = new JFileChooser();
                        // Filter
                        FileNameExtensionFilter filter = 
                                new FileNameExtensionFilter("PDF docs", "pdf");
                        chooser.setFileFilter(filter);

                        int returnVal = chooser.showOpenDialog(MainFrame.this);
                        
                        if(returnVal == JFileChooser.APPROVE_OPTION) {
                            File file = chooser.getSelectedFile();
                            load(file.toURI());
                        }
                    }
                });
        
        menu.add("Load URL").addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        UrlLoaderDialog dialog = new UrlLoaderDialog(MainFrame.this);
                        dialog.setVisible(true); // block's till closed
                    }
                });

        menu.addSeparator();

        menu.add("Show reader window").addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        showReaderWindow();
                    }
                });
        
        menu.addSeparator();

        menu.add("Exit").addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });        
        
        return menu;
    }
    
    void load(URI uri) {
        LoaderDialog dialog = new LoaderDialog(this, uri);
        dialog.setVisible(true);// Blocks till loaded
    }

    /** Callback from LoaderDialog */
    void loading(URI uri) throws IOException {

        String scheme = uri.getScheme();
        //System.out.println("URI schema: " + scheme);

        switch(scheme) {
        case "http":
            loadUrl(uri);
            break;
        case "file":
            String path = uri.getPath();
            //System.out.println("Path: " + path);
            if (path.endsWith(".pdf")) {
                loadPdfFile(uri);
            } else {
                loadTextFile(uri);
            }
            break;
        default:
            System.out.println("Can handle this: " + uri);
        }
    }

    void loadPdfFile(URI uri) {

        try (PDDocument document = PDDocument.load(uri.toURL())) {
            
            PDFTextStripper stripper = new PDFTextStripper();
            String text = stripper.getText(document);
            setText(text);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    void loadTextFile(URI uri) {
        
        Path path = Paths.get(uri);
        try {
            List<String> allLines = Files.readAllLines(path, ENCODING);
            StringBuilder sb = new StringBuilder();
            for (String line : allLines) {
                sb.append(line).append('\n');
            }
            String text = sb.toString();
            setText(text);
            
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }    
    
    void loadUrl(URI uri) throws IOException {
        Document doc = Jsoup.connect(uri.toURL().toString()).get();
        Element body = doc.body();
        // Get all the <p> elements
        Elements elements = body.select("p");
        StringBuilder sb = new StringBuilder();
        for (Element element : elements) {
            sb.append(element.text()).append("\n\n");
        }
        // SEt text
        setText(sb.toString());   
    }
    
    void setText(String text) {
        // Load text in the main window
        textarea.setText(text);
        textarea.setCaretPosition(0);
        // ... if needed
        showReaderWindow();
        // Load text also in the reader window
        reader.reload();
    }
}
