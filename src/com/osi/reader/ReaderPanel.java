package com.osi.reader;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Scanner;

import javax.swing.JPanel;


/**
 * Panel where words are shown
 */
class ReaderPanel extends JPanel {
    
    private ReaderWindow parent;
    
    private Scanner scanner;
    private MyReadable readable;
    
    
    /** constructor */
    ReaderPanel(ReaderWindow parent) {
        this.parent = parent;
    }
    
    void setText(String text) {
        // Need to track position
        this.readable = new MyReadable(text);
        this.scanner = new Scanner(readable);
        // First word
        showNextWord();
    }
    
    @Override
    protected void paintComponent(Graphics graphics) {
        // Background
        paintBackground(graphics);
        // Foreground (text)
        paintText(graphics);
    }
    
    private void paintBackground(Graphics graphics) {
        
        // Creates a copy of the current graphics component
        Graphics2D g = (Graphics2D) graphics.create();
        
        // Object total dimensions
        Dimension dim = this.getSize();
        
        int rightShift = dim.width / 4; 
        
        // Background (always opaque)
        g.setColor(getBackground());
        g.fillRect(0, 0, dim.width, dim.height);
        
        // Border
        
        g.setStroke(new BasicStroke(3));
        
        g.setColor(Color.BLACK);
        g.drawRect(0,  0, dim.width - 1, dim.height - 1);
        
        // Char mark
        int x = (dim.width / 2) - rightShift + 10;
        g.drawLine(x, 0, x, 8);
        g.drawLine(x, dim.height - 8, x, dim.height);
    }
    
    private void paintText(Graphics graphics) {
        
        if (currentWord == null) {
            return;
        }
        
        // Creates a copy of the current graphics component
        Graphics2D g = (Graphics2D) graphics.create();

        // Object total dimensions
        Dimension dim = this.getSize();

        int rightShift = dim.width / 4; 
        
        // Font
        int fontSize = (int) (dim.height * 0.70); 
        Font font = new Font("Arial", Font.PLAIN, fontSize); // 50
        g.setFont(font);
        // Antialiasing
        g.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        // Text
        // Center char in RED
        int index = currentWord.length() / 4;
        if (index == 0 && currentWord.length() > 1) index = 1;
        String letter = String.valueOf(currentWord.charAt(index));
        int shift;
        if (index > 0) {
            String substring = currentWord.substring(0, index);
            shift = textWidth(substring, graphics, font);
        } else {
            shift = 0;
        }

        // Text
        g.setColor(getForeground());
        int x = (dim.width / 2) - rightShift - shift;
        //int y = dim.height - 20;
        int y = (int) (dim.height * 0.70);
        g.drawString(currentWord, x, y);
        
        // Red char
        g.setColor(Color.RED);
        g.drawString(letter, x + shift, y);
    }
    
    private int textWidth(String text, Graphics graphics, Font font) {
        FontMetrics metrics = graphics.getFontMetrics(font);
        return metrics.stringWidth(text);
    }
    
    private String currentWord;
    private int delay;
    
    /** Next word in text starting from index */
    public void showNextWord() {
        
        // Keep the previous word if a delay is in place
        if (delay > 0) {
            --delay;
            return;
        }
        
        // Next word
        if (scanner != null && scanner.hasNext()) {
            this.currentWord = scanner.next();
            // Set custom's word delay
            setDelay(currentWord);
            repaint();
        } else {
            parent.pause(true); // Leave last word :)
        }
        
    }
    
    /** Add specific delays for certain words */
    private void setDelay(String word) {
        this.delay = 0;
        if (word != null) {
            if (word.endsWith(",")) {
                this.delay = 1;
            } else if (word.endsWith(";")) {
                this.delay = 2;
            } else if (word.endsWith(".")) {
                this.delay = 4;
            }
        }
    }

    public String getCurrentWord() {
        return (currentWord) != null ? currentWord : "";
    }
    
    public int getBytesRead() {
        return (readable != null) ? readable.getPosition() : 0;
    }
    
    
    
    
    /** Needed to track position into the text */
    class MyReadable implements Readable {

        private String text;
        private int position;
        
        /** constructor */
        public MyReadable(String text) {
            this.text = text;
        }
        
        @Override
        public int read(CharBuffer cb) throws IOException {
            if (text.length() > position) {
                cb.append(text.charAt(position));
                ++position;
                return 1;
            } else {
                return -1;
            }
        }
        
        public int getPosition() {
            return position;
        }
    }
}