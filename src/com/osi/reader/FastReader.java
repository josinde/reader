package com.osi.reader;


import javax.swing.SwingUtilities;


/**
 * Application entry point
 * @author josinde 
 */
public class FastReader {

    /** Main method */
    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    // Main frame
                    MainFrame frame = new MainFrame();
                    frame.setVisible(true);
                    
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

    }
}
