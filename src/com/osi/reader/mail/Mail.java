package com.osi.reader.mail;

import java.util.*;

import javax.mail.*;

public class Mail {
    
    public static void main(String[] args) {
        
        
        String user = "jose.osinde@sciops.esa.int";
        String password = "***";
        
        Properties props = new Properties();
        
        props.setProperty("mail.debug", "false");
       
        props.setProperty("mail.store.protocol", "imaps");
        props.setProperty("mail.imaps.ssl.enable", "true");
        props.setProperty("mail.imaps.ssl.socketFactory.class", "com.osi.reader.mail.DummySSLSocketFactory");
        props.setProperty("mail.imaps.ssl.socketFactory.fallback", "false");       

        
        try {
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();
            store.connect("imap.sciops.esa.int", user, password);
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);
            int size = inbox.getMessageCount();
            for (int index = size; index <= size; ++index) { // take the last one
                
                Message msg = inbox.getMessage(index);
                
                // From
                Address[] in = msg.getFrom();
                for (Address address : in) {
                    System.out.println("FROM: " + address.toString());
                }
                // Subject
                System.out.println("SUBJECT: " + msg.getSubject());
                // Date
                System.out.println("DATE: " + msg.getSentDate() + " -> " + msg.getReceivedDate());
                // Content
                System.out.println("CONTENT:");
                Object data = msg.getContent();
                if (data instanceof String) {
                    System.out.println(data);
                } else if (data instanceof Multipart) {
                    Multipart mp = (Multipart) msg.getContent();
                    BodyPart bp = mp.getBodyPart(0);
                    System.out.println(bp.getContent());
                } else {
                    System.out.println("DON'T KNOW HOW TO HANDLE THIS: " + data.getClass());
                }
            }
        } catch (Exception mex) {
            mex.printStackTrace();
        }
    }
    
}